package be.josomers.takephoto.util;

import android.net.Uri;

import java.io.File;

import static android.net.Uri.fromFile;
import static android.os.Environment.DIRECTORY_PICTURES;
import static android.os.Environment.getExternalStoragePublicDirectory;
import static java.io.File.separator;
import static org.joda.time.DateTime.now;

/**
 * @author Jo Somers
 */
public class MediaHelper {

    public static final int MEDIA_TYPE_IMAGE = 1;

    public static final int MEDIA_TYPE_ID_CARD_IMAGE = 2;

    /**
     * Get output media file
     *
     * @param directoryName name of the directory used
     * @param type          video or image
     * @return {@link android.net.Uri} of the file
     */
    public static Uri getOutputMediaFileUri(
            final String directoryName,
            final int type) {

        final File tempFile = getOutputMediaFile(directoryName, type);
        return (tempFile != null) ? fromFile(tempFile) : null;
    }

    /**
     * Get output media file
     *
     * @param directoryName name of the directory used
     * @param type          video or image
     * @return {@link java.io.File}
     */
    private static File getOutputMediaFile(
            final String directoryName,
            final int type) {

        // External sd-card location

        final File mediaStorageDir = new File(getExternalStoragePublicDirectory(DIRECTORY_PICTURES), directoryName);

        // Create the storage directory if it does not exist

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        // Create a media file name

        final String dateFormat = now().toString("yyyyMMdd_HHmmss");

        switch (type) {
            case MEDIA_TYPE_IMAGE:
            case MEDIA_TYPE_ID_CARD_IMAGE:
                return new File(mediaStorageDir.getPath() + separator + "IMG_" + dateFormat + ".jpg");
            default:
                return null;
        }
    }

}
