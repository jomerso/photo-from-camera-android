package be.josomers.takephoto.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;

import java.io.File;

import be.josomers.takephoto.R;

import static android.content.Intent.ACTION_GET_CONTENT;
import static android.content.Intent.ACTION_MEDIA_SCANNER_SCAN_FILE;
import static android.content.Intent.ACTION_PICK;
import static android.content.Intent.EXTRA_INITIAL_INTENTS;
import static android.content.Intent.createChooser;
import static android.content.pm.PackageManager.FEATURE_CAMERA;
import static android.net.Uri.fromFile;
import static android.provider.MediaStore.ACTION_IMAGE_CAPTURE;
import static android.provider.MediaStore.EXTRA_OUTPUT;
import static android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
import static be.josomers.takephoto.util.MediaHelper.MEDIA_TYPE_IMAGE;
import static be.josomers.takephoto.util.MediaHelper.getOutputMediaFileUri;

/**
 * @author Jo Somers
 */
public class SelectPhotoUtil {

    /**
     * Refresh the phone's gallery
     *
     * @param activity {@link android.app.Activity}
     * @param fileUri  {@link android.net.Uri} to load the image from
     */
    public static void refreshPhoneGallery(
            final Activity activity,
            final Uri fileUri) {

        if (fileUri.getPath() != null) {
            final Uri contentUri = fromFile(new File(fileUri.getPath()));
            final Intent mediaScanIntent = new Intent(ACTION_MEDIA_SCANNER_SCAN_FILE);
            mediaScanIntent.setData(contentUri);
            activity.sendBroadcast(mediaScanIntent);
        }

    }

    /**
     * Select a photo from the gallery or the phone's camera
     *
     * @param source             {@link android.app.Fragment}
     * @param REQUEST_CODE_IMAGE {@link Integer}
     * @return created {@link android.net.Uri}
     */
    public static Uri selectPhotoFromCameraOrGallery(
            final Activity source,
            final int REQUEST_CODE_IMAGE) {

        Uri fileUri = null;

        final Intent galleryIntent = new Intent(ACTION_PICK, EXTERNAL_CONTENT_URI);
        galleryIntent.setAction(ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");

        final Intent chooserIntent = createChooser(galleryIntent, source.getString(R.string.tChoosePhoto));

        if (hasCamera(source)) {
            final Intent captureIntent = new Intent(ACTION_IMAGE_CAPTURE);
            fileUri = getOutputMediaFileUri("TakePhoto", MEDIA_TYPE_IMAGE);
            captureIntent.putExtra(EXTRA_OUTPUT, fileUri);
            chooserIntent.putExtra(EXTRA_INITIAL_INTENTS,
                    new Intent[]{
                            captureIntent
                    }
            );
        }

        source.startActivityForResult(chooserIntent, REQUEST_CODE_IMAGE);

        return fileUri;
    }

    /**
     * Check if the user's phone has a camera available.
     *
     * @param context {@link android.content.Context}
     * @return true or false, camera available or not.
     */
    public static boolean hasCamera(final Context context) {
        final PackageManager packageManager = context.getPackageManager();
        return packageManager != null && packageManager.hasSystemFeature(FEATURE_CAMERA);
    }

}
