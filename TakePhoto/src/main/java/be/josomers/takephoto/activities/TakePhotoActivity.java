package be.josomers.takephoto.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import be.josomers.takephoto.R;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

import static android.content.Intent.ACTION_SEND;
import static android.content.Intent.EXTRA_STREAM;
import static android.content.Intent.EXTRA_SUBJECT;
import static android.content.Intent.createChooser;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static be.josomers.takephoto.util.SelectPhotoUtil.hasCamera;
import static be.josomers.takephoto.util.SelectPhotoUtil.refreshPhoneGallery;
import static be.josomers.takephoto.util.SelectPhotoUtil.selectPhotoFromCameraOrGallery;


public class TakePhotoActivity extends Activity {

    private static final int RESULT_CODE_CANCELLED = 0;

    private static final int REQUEST_CODE_SELECT_IMAGE = 1;

    @InjectView(R.id.photo)
    ImageView photoImageView;

    @InjectView(R.id.tabToSelect)
    View tabToSelectView;

    @InjectView(R.id.sendPhotoGroup)
    View sendView;

    private Uri fileUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_photo);
        ButterKnife.inject(this);

        photoImageView.setImageURI(null);
        sendView.setVisibility(GONE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_SELECT_IMAGE
                && resultCode != RESULT_CODE_CANCELLED) {

            if (data != null
                    && data.getData() != null) {

                fileUri = data.getData();
            }

            refreshPhoneGallery(this, fileUri);
            loadPictureFromFileUrl();
        }
    }

    @OnClick(R.id.photo)
    public void onTakePhotoClicked() {
        if (hasCamera(this)) {
            fileUri = selectPhotoFromCameraOrGallery(this, REQUEST_CODE_SELECT_IMAGE);
        } else {
            Toast.makeText(this, getString(R.string.tCameraNA), Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.sendPhoto)
    public void onSendPhotoButtonClicked() {
        final Intent intent = new Intent(ACTION_SEND);
        intent.putExtra(EXTRA_SUBJECT, getString(R.string.tEmailSubject));
        intent.putExtra(EXTRA_STREAM, fileUri);
        intent.setType("application/image");

        startActivity(createChooser(intent, getString(R.string.tSendEmail)));
    }

    private void loadPictureFromFileUrl() {
        final Callback callback = new Callback() {
            @Override
            public void onSuccess() {
                tabToSelectView.setVisibility(GONE);
                sendView.setVisibility(VISIBLE);
            }

            @Override
            public void onError() {
                tabToSelectView.setVisibility(VISIBLE);
                sendView.setVisibility(GONE);
            }
        };

        Picasso.with(this)
                .load(fileUri)
                .placeholder(R.drawable.photo_loader_loading)
                .error(R.drawable.photo_loader_error)
                .into(photoImageView, callback);

        tabToSelectView.setVisibility(GONE);
    }

}
