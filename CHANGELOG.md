Change Log
==========

Version 1.0 *(2014-04-04)*
--------------------------
* Select an image from gallery or phone, show it center cropped
* Share the image as an attachment via email
* Gradle project setup